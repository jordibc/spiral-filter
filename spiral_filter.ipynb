{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "wooden-basin",
   "metadata": {},
   "source": [
    "# Spiral Filter"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "comprehensive-diana",
   "metadata": {},
   "source": [
    "So, what is the \"spiral filter\"? How does it work?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "periodic-geography",
   "metadata": {},
   "source": [
    "Let's look at three examples, in 1D, 2D and 3D:\n",
    "\n",
    "$$\n",
    "\\newcommand{\\norm}[1]{\\mathop{\\|#1\\|}}\n",
    "\\newcommand{\\vect}[1]{\\boldsymbol{#1}}\n",
    "\\newcommand{\\sign}[1]{\\mathop{\\text{sign}(#1)}}\n",
    "s(x), s(x, y), s(x, y, z)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "rough-rolling",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "mature-auditor",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "\n",
    "import numpy as np\n",
    "from numpy import (\n",
    "    linspace, pi, exp, sqrt, sin, cos, sinc, real, imag, abs, angle, arctan2,\n",
    "    sum, meshgrid, r_, nan_to_num, errstate)\n",
    "from numpy.fft import fft, ifft, fftn, ifftn, fftshift, ifftshift, fftfreq\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "from mpl_toolkits.mplot3d import axes3d"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "incoming-robert",
   "metadata": {},
   "source": [
    "## 1D Example - Gaussian Exponential"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "beneficial-trading",
   "metadata": {},
   "source": [
    "Imagine we have a \"wave packet\", a gaussian exponential $s(x) = e^{-x^2} \\cos(10 \\, x)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "interstate-answer",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = linspace(-5, 5, 1000)\n",
    "sx = exp(-x**2) * cos(10*x)\n",
    "\n",
    "# Other interesting examples:\n",
    "#   sx = exp(-x**2) * (cos(10*x) + cos(12*x))\n",
    "#   sx = exp(-(x+1.00)**2) * cos(10*x) + exp(-(x-1)**2) * cos(12*x)\n",
    "#   sx = exp(-(x-0.03)**2) * cos(10*x) + exp(-(x+0.03)**2) * cos(12*x)\n",
    "#   sx = exp(-(x-0.02)**2) * cos(10*x) + exp(-(x+0.02)**2) * cos(12*x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "experienced-event",
   "metadata": {},
   "source": [
    "Since we are going to do a few 1D plots, let's create a shortcut (a convenience function) and use it too see our signal $s$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "interested-desert",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot1d(x, sx, xlabel=None, label=None, newfig=True, **kwargs):\n",
    "    if newfig:\n",
    "        plt.figure(figsize=(9, 4))\n",
    "        \n",
    "    plt.plot(x, sx, label=label, **kwargs)\n",
    "\n",
    "    if xlabel:\n",
    "        plt.xlabel(xlabel)\n",
    "    \n",
    "    plt.legend();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "crazy-protein",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot1d(x, sx, '$x$', '$s(x)$')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "palestinian-group",
   "metadata": {},
   "source": [
    "We can find its frequency content using the Fourier transform $\\hat{s} = \\mathcal{F}(s)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "announced-keeping",
   "metadata": {},
   "outputs": [],
   "source": [
    "freq = fftfreq(len(x), d=0.05)\n",
    "sf = fft(sx)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "different-camping",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot1d(fftshift(freq), fftshift(abs(sf)), '$f$', '$|\\\\hat{s}(f)|$')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "unauthorized-computer",
   "metadata": {},
   "source": [
    "### Monogenic Signal"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "documentary-collins",
   "metadata": {},
   "source": [
    "The *monogenic signal* $s_m$ associated to a given signal $s$ is basically the same as the original signal, but without any negative frequencies. (See [the wikipedia entry on monogenic signals](https://en.wikipedia.org/wiki/Analytic_signal#The_monogenic_signal) for more information.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "collect-quebec",
   "metadata": {},
   "source": [
    "$\\hat{s_m}(f) = (1 + \\sign{f}) \\, \\hat{s}(f)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "confirmed-feeling",
   "metadata": {},
   "outputs": [],
   "source": [
    "def monogen(sx):\n",
    "    sf = fft(sx)\n",
    "    sf[freq < 0] = 0\n",
    "    sf[freq > 0] *= 2\n",
    "    return ifft(sf)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "outside-clark",
   "metadata": {},
   "outputs": [],
   "source": [
    "m = monogen(sx)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "living-villa",
   "metadata": {},
   "source": [
    "The monogenic signal encodes in its real part the original signal, and in its imaginary part a signal that is \"shifted by $\\pi/2$\" (in its frequencies)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "delayed-andrew",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(9, 4))\n",
    "plot1d(x, real(m), '$x$', '$\\\\Re(s_m(x))$', newfig=False)\n",
    "plot1d(x, imag(m), '$x$', '$\\\\Im(s_m(x))$', newfig=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "departmental-cancer",
   "metadata": {},
   "source": [
    "So its abs is the \"envelope\" signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "minute-skiing",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(9, 4))\n",
    "plot1d(x, sx, '$x$', '$s(x) = \\\\Re(s_m(x))$', newfig=False)\n",
    "plot1d(x, imag(m), '$x$', '$\\\\Im(s_m(x))$', newfig=False, alpha=0.5)\n",
    "plot1d(x, abs(m), '$x$', '$|s_m(x)|$', newfig=False, alpha=1)\n",
    "plot1d(x, angle(m)/pi, '$x$', '$\\\\phi(s_m(x)) / \\\\pi$', newfig=False, alpha=0.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "brilliant-notice",
   "metadata": {},
   "source": [
    "#### Phase vs cos(phase)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "arranged-accountability",
   "metadata": {},
   "source": [
    "Can the phase be not well defined? That is, could it be that its sign is unknown? Then we would expect to see jumps in the phase $\\phi$, but $\\cos(\\phi)$ would still be continuous. Let's see."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "curious-expense",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot1d(x, cos(angle(m)), '$x$', '$\\\\cos(\\\\phi)$')\n",
    "plot1d(x, angle(m)/pi, '$x$', '$\\\\phi / \\\\pi$', newfig=False, alpha=0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "therapeutic-physiology",
   "metadata": {},
   "source": [
    "### Spiral Filter"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "mighty-jewelry",
   "metadata": {},
   "source": [
    "An alternative way to construct the monogenic signal is to first get the original signal \"shifted by $\\pi/2$\". This can be done by changing the sign of negative frequencies, times a multiple of $i$ so that its inverse remains real (the standard chosen is $-i$, thus changing $\\cos$ to $\\sin$)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "occupational-planner",
   "metadata": {},
   "source": [
    "$\\hat{s}(f) \\rightarrow -i \\sign{f} \\hat{s}(f)$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "utility-lesson",
   "metadata": {},
   "source": [
    "We can see this as the application of a filter, the *spiral filter* $h$, that is, the convolution $h * s$, which in the fourier domain is just a multiplication by:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "taken-application",
   "metadata": {},
   "source": [
    "$\\hat{h}(f) = -i \\sign{f}$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "understanding-quick",
   "metadata": {},
   "source": [
    "The monogenic signal will be simply a signal with the same original real part, and an imaginary part that corresponds to the shifted signal:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "covered-michael",
   "metadata": {},
   "source": [
    "$s_m(x) = s(x) + i \\, (h * s)(x)$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "photographic-poetry",
   "metadata": {},
   "source": [
    "It is easy to see that this is equivalent to the previous definition, since $\\hat{s_m}(f) = \\hat{s}(f) + i \\, (-i \\sign{f} \\hat{s}(f)) = (1 + \\sign{f}) \\, \\hat{s}(f)$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "simplified-exhibition",
   "metadata": {},
   "source": [
    "## 2D Example"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "charitable-rental",
   "metadata": {},
   "source": [
    "There is no unique way to generalize the \"setting negative frequencies to 0\" that we used to define the monogenic signal in 1D. One way to generalize it is to change $\\sign{f} \\rightarrow$ $\\frac{\\vect{f}}{\\norm{\\vect{f}}}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "proof-marijuana",
   "metadata": {},
   "source": [
    "Thus, in 2D, we can define the spiral filter as\n",
    "\n",
    "$$\n",
    "\\hat{h}(\\vect{f}) = -i \\frac{\\vect{f}}{\\norm{\\vect{f}}}\n",
    "$$\n",
    "\n",
    "where $\\vect{f} = (f_x, f_y)$ is the vector with the two spatial frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "tutorial-benjamin",
   "metadata": {},
   "outputs": [],
   "source": [
    "def grid2D(n):\n",
    "    \"Return a n*n grid centered at 0\"\n",
    "    coords = r_[:n] - n // 2  # n=4 -> [-2, -1, 0, 1]\n",
    "    return meshgrid(coords, coords)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "united-intervention",
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 150\n",
    "\n",
    "x, y = grid2D(n)\n",
    "\n",
    "r = sqrt(x**2 + y**2)\n",
    "\n",
    "# One interesting example would be\n",
    "#   sxy = exp(-r**2 / 1000) * cos(0.4 * r + 0.2)\n",
    "# but let's see instead this:\n",
    "\n",
    "kx, ky = 5, 0\n",
    "sxy = exp(-r**2 / 1000) * cos(0.1*kx * x + 0.1*ky * y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "outstanding-fifth",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot2d(x, y, sxy, xlabel='$x$', ylabel='$y$', zlabel='$s(x, y)$'):\n",
    "    ax = plt.figure(figsize=(9, 6)).add_subplot(projection='3d')\n",
    "    ax.plot_surface(x, y, sxy, cmap='coolwarm')\n",
    "    ax.set_xlabel(xlabel)\n",
    "    ax.set_ylabel(ylabel)\n",
    "    ax.set_zlabel(zlabel)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "tropical-specification",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot2d(x, y, sxy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "informational-henry",
   "metadata": {},
   "outputs": [],
   "source": [
    "def spiral_filter2D(n):\n",
    "    \"Return the freq-domain spiral filter for the two dimensions (x, y)\"\n",
    "    fx, fy = grid2D(n)\n",
    "    \n",
    "    freq = sqrt(fx**2 + fy**2)\n",
    "    \n",
    "    def H(fi):\n",
    "        return ifftshift(-1j * nan_to_num(fi / freq))\n",
    "    \n",
    "    with errstate(invalid='ignore'):  # ignore warning about dividing by 0 in one bin\n",
    "        return H(fx), H(fy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "athletic-prompt",
   "metadata": {},
   "outputs": [],
   "source": [
    "Hx, Hy = spiral_filter2D(n)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "intended-olympus",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot2d_2(x, y, s1xy, s2xy, xlabel='$x$', ylabel='$y$', zlabel1='$s_1(x, y)$', zlabel2='$s_2(x, y)$'):\n",
    "    fig = plt.figure(figsize=(9, 4))\n",
    "\n",
    "    axx = fig.add_subplot(121, projection='3d')\n",
    "    axx.plot_surface(x, y, s1xy, cmap='coolwarm')\n",
    "    axx.set_xlabel(xlabel)\n",
    "    axx.set_ylabel(ylabel)\n",
    "    axx.set_zlabel(zlabel1)\n",
    "\n",
    "    axy = fig.add_subplot(122, projection='3d')\n",
    "    axy.plot_surface(x, y, s2xy, cmap='coolwarm')\n",
    "    axy.set_xlabel(xlabel)\n",
    "    axy.set_ylabel(ylabel)\n",
    "    axy.set_zlabel(zlabel2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bright-bread",
   "metadata": {},
   "outputs": [],
   "source": [
    "fx, fy = grid2D(n)\n",
    "plot2d_2(  fx,      fy,    fftshift(real(1j * Hx)), fftshift(real(1j * Hy)),\n",
    "         '$f_x$', '$f_y$', '$i H_x$',               '$i H_y$')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "western-northwest",
   "metadata": {},
   "source": [
    "Let's see the action of the spiral filter for each coordinate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "present-radio",
   "metadata": {},
   "outputs": [],
   "source": [
    "sHx = ifftn(Hx * fftn(sxy))\n",
    "sHy = ifftn(Hy * fftn(sxy))\n",
    "\n",
    "EPSILON = 1e-3\n",
    "assert sum(abs(imag(sHx))) < EPSILON * sum(abs(real(sHx)))\n",
    "assert sum(abs(imag(sHy))) < EPSILON * sum(abs(real(sHy)))\n",
    "\n",
    "plot2d_2(  x,     y,     real(sHx),   real(sHy),\n",
    "         '$x$', '$y$', '$h_x * s$', '$h_y * s$')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efficient-mainstream",
   "metadata": {},
   "source": [
    "The amplitude of the monogenic signal will be:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bored-short",
   "metadata": {},
   "outputs": [],
   "source": [
    "sA = sqrt(abs(sxy)**2 + abs(sHx)**2 + abs(sHy)**2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "recreational-progress",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot2d(x, y, sA, '$x$', '$y$', '$|s_m(x, y)|$')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "extended-trigger",
   "metadata": {},
   "source": [
    "Now let's see all together:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "mature-yahoo",
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = plt.figure(figsize=(9, 9)).add_subplot(projection='3d')\n",
    "ax.set_xlabel('$x$')\n",
    "ax.set_ylabel('$y$')\n",
    "ax.set_box_aspect((1,1,2))\n",
    "for i, s in enumerate([sxy, real(sHx), real(sHy), sA]):\n",
    "    ax.plot_surface(x, y, 3*i + s, cmap='coolwarm')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "disturbed-kingston",
   "metadata": {},
   "source": [
    "### Direction and phase"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "figured-shape",
   "metadata": {},
   "outputs": [],
   "source": [
    "sN = [arctan2(real(sH), sxy) for sH in [sHx, sHy]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "white-significance",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.quiver(x, y, sN[0], sN[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "minute-viking",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.imshow(sN[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "approved-scholarship",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.imshow(sN[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "classical-paper",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot2d_2(x, y, sN[0], sN[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "radical-processing",
   "metadata": {},
   "outputs": [],
   "source": [
    "sPhi = arctan2(sqrt(abs(sHx)**2 + abs(sHy)**2), sxy)\n",
    "# which is the same as the norm of sN"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dominant-listing",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot2d(x, y, sPhi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "crude-coast",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot2d(x, y, cos(sPhi))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "located-reader",
   "metadata": {},
   "source": [
    "## 3D Volume"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "initial-violence",
   "metadata": {},
   "outputs": [],
   "source": [
    "%run ../cryoem-common/cryoem_common.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "enormous-horizontal",
   "metadata": {},
   "outputs": [],
   "source": [
    "vol1, voxel_n1, voxel_size1 = get_vol_and_voxel('emd_10418_half_map_1.map')\n",
    "vol2, voxel_n2, voxel_size2 = get_vol_and_voxel('emd_10418_half_map_2.map')\n",
    "\n",
    "assert voxel_size1 == voxel_size2 and voxel_n1 == voxel_n2\n",
    "voxel_size = voxel_size1\n",
    "voxel_n = voxel_n1\n",
    "\n",
    "V = (vol1 + vol2) / 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "italian-commitment",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(V)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "unknown-monday",
   "metadata": {},
   "outputs": [],
   "source": [
    "def shell(f):\n",
    "    \"Return a normalized shell of spatial frequencies, around frequency f\"\n",
    "    S = exp(- (f_norm - f)**2 / (2 * f_width**2))\n",
    "    return S / sum(S)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "comprehensive-stable",
   "metadata": {},
   "outputs": [],
   "source": [
    "voxel_width_of_selected_freq = 4.6  # width (in voxels) of the shell\n",
    "f_width = voxel_width_of_selected_freq / (voxel_n * voxel_size)  # frequency width"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "constant-dating",
   "metadata": {},
   "outputs": [],
   "source": [
    "FV = fftn(V)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "enabling-python",
   "metadata": {},
   "outputs": [],
   "source": [
    "Hx, Hy, Hz = spiral_filter(voxel_n, voxel_size)\n",
    "\n",
    "hxv = ifftn(Hx * FV)\n",
    "hyv = ifftn(Hy * FV)\n",
    "hzv = ifftn(Hz * FV)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "floppy-encoding",
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitude = sqrt(abs(V)**2 + abs(hxv)**2 + abs(hyv)**2 + abs(hzv)**2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "concrete-methodology",
   "metadata": {},
   "outputs": [],
   "source": [
    "sum(abs(real(hxv)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "impressed-strap",
   "metadata": {},
   "source": [
    "Look at https://stackoverflow.com/questions/61596925/quiver-3d-python-matplotlib-how-to-get-same-color-for-arrow-shaft-and-arrow-h"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
