# Spiral Filter

Notes and examples on how the "spiral filter" works.


## Dependencies

In addition to having Jupyter installed, for some parts this notebook
requires:

- Having https://gitlab.com/jordibc/cryoem-common downloaded in its
  parent directory.
- Having the modules `ipympl` for embedded matplotlib graphics, and
  `mrcfile` to read 3D volume data (`pip install ipympl mrcfile`).
- Having the half-volume files
  [emd_10418_half_map_1.map](https://ftp.ebi.ac.uk/pub/databases/emdb/structures/EMD-10418/other/emd_10418_half_map_1.map.gz)
  and
  [emd_10418_half_map_2.map](https://ftp.ebi.ac.uk/pub/databases/emdb/structures/EMD-10418/other/emd_10418_half_map_2.map.gz)
  for a [protein example](https://www.ebi.ac.uk/emdb/EMD-10418) from
  the [EMDB](https://www.ebi.ac.uk/emdb/).


## Running

To open the notebook:

```sh
jupyter notebook
```
